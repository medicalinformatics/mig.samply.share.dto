package de.samply.share.utils.xmltocsv;

import java.io.IOException;
import java.io.Writer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;

public class PatientTable implements Comparable<PatientTable> {

  public final String patientID;
  public final String location;
  public HashMap<Identifier, Element> caseElements = new HashMap<>();
  //TreeMap uses the sorting by date, HashMap does not
  public TreeMap<Date, Episode> episodes = new TreeMap<>();

  public PatientTable(String patientID, String location) {
    this.patientID = patientID;
    this.location = location;
  }

  /**
   * this patient adds new elements for the case.
   */
  public void addCaseElement(Identifier id, String value, String formID) {
    Element element = caseElements.get(id);
    if (element == null) {
      element = new Element(id);
      caseElements.put(id, element);
    }
    element.addValue(value, formID);
  }

  /**
   * this patient adds new elements for the Episode.
   */
  public void addEpisodeElement(String epiDate, Identifier id, String value, String formID) {
    if (epiDate == null) {
      return;
    }
    SimpleDateFormat format = dataFormat(epiDate);
    Date date;
    try {
      date = format.parse(epiDate);
      Episode episode = episodes.get(date);
      if (episode == null) {
        episode = new Episode(epiDate);
        episodes.put(date, episode);
      }
      episode.addElement(id, value, formID);
    } catch (ParseException e) {
      e.printStackTrace();
    }
  }

  private SimpleDateFormat dataFormat(String epiDate) {
    if (epiDate.contains("-")) {
      return new SimpleDateFormat("yyyy-MM-dd");
    } else if (epiDate.contains("/")) {
      return new SimpleDateFormat("dd/MM/yyyy");
    } else {
      return null; //Exception
    }
  }

  /**
   * this patient is written in Csv.
   */
  public void write(Headers headers, Writer csvWriter) throws IOException {
    int x = headers.size();
    int y = 0;
    for (Episode episode : episodes.values()) {
      y += episode.maxIndex;
    }
    for (Element element : caseElements.values()) {
      if (y < element.values.size()) {
        y = element.values.size();
      }
    }

    String[][] table = new String[y][x];//is stable and faster than List
    //write PID in the table
    for (int i = 0; i < y; i++) {
      table[i][0] = String.valueOf(patientID);
      table[i][1] = String.valueOf(location);
    }

    //table of case
    x = headers.casePosition();
    for (Identifier id : headers.caseElementIDs) {
      Element element = caseElements.get(id);
      if (element != null) {
        if (Configuration.FILL_ALL) {
          for (int i = 0; i < y; i++) {
            if (i < element.values.size()) {
              Value v = element.values.get(i);
              table[i][x] = v.value + v.is.note;

            } else if (element.name.isRepeatable) {
              //nothing

            } else if (0 < element.values.size()) {
              //fill same value in empty row
              Value v = element.values.get(0);
              table[i][x] = v.value + v.is.note;

            }
          }

        } else {
          for (int i = 0; i < element.values.size(); i++) {
            Value v = element.values.get(i);
            table[i][x] = v.value + v.is.note;

          }
        }
      }
      x++;
    }

    //table of episode
    x = headers.episodePosition();
    y = 0;
    int whereIsEpiDate = 2; //x - 1;

    for (Episode episode : episodes.values()) {
      //write epi-date in the table
      for (int i = y; i < y + episode.maxIndex; i++) {
        table[i][whereIsEpiDate] = episode.epiDate;
      }
      //write epi-elements in the table
      for (Identifier id : headers.epiElementIDs) {
        Element element = episode.epiElements.get(id);
        if (element != null) {

          if (Configuration.FILL_ALL) {
            for (int i = 0; i < episode.maxIndex; i++) {

              if (i < element.values.size()) {
                Value v = element.values.get(i);
                table[y + i][x] = v.value + v.is.note;

              } else if (element.name.isRepeatable) {
                //nothing

              } else if (0 < element.values.size()) { //fill same value in empty row
                Value v = element.values.get(0);
                table[y + i][x] = v.value + v.is.note;

              }
            }
          } else {
            for (int i = 0; i < element.values.size(); i++) {
              Value v = element.values.get(i);
              table[y + i][x] = v.value + v.is.note;
            }
          }
        }
        x++;
      }
      x = headers.episodePosition(); //reset of x
      y += episode.maxIndex; // y of next episode
    }

    //write this table into csvWriter
    for (y = 0; y < table.length; y++) {
      StringBuilder line = new StringBuilder();
      for (x = 0; x < table[y].length; x++) {
        String word = table[y][x];
        if (word != null) {
          line.append(CsvFormat.getWord(word));
        } else {
          line.append(CsvFormat.getWord(""));
        }
      }
      csvWriter.append(CsvFormat.getLine(line));
    }
  }

  /**
   * TreeSet uses the compareTo that is important for order.
   */
  @Override
  public int compareTo(PatientTable patientTable) {
    return patientID.compareTo(patientTable.patientID);
  }
}