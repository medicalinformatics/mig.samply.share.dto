package de.samply.share.utils.xmltocsv;

public class Value {
  enum Is {
    DUPLICATE(" [duplicate]"), CONFLICT(" [conflict]"), REPEATABLE(""/*" [repeatable]"*/), NORMAL(
        "");
    public String note;

    Is(String note) {
      this.note = note;
    }
  }

  public String value;
  public Value.Is is;

  public Value(String value, Value.Is is) {
    this.value = value;
    this.is = is;
  }
}
