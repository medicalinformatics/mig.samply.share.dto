package de.samply.share.utils.xmltocsv;

import java.io.IOException;
import java.io.Writer;
import java.util.TreeSet;

public class Table {
  public Headers headers = new Headers();
  public TreeSet<PatientTable> patients = new TreeSet<>();

  /**
   * table will be created and written in csv.
   */
  public void create(Writer csvWriter) throws IOException {
    headers.write(csvWriter);
    for (PatientTable patientTable : patients) {
      patientTable.write(headers, csvWriter);
    }
  }
}
