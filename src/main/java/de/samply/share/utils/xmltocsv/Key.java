package de.samply.share.utils.xmltocsv;

public class Key {

  public final String mdrName;
  public final String recordName;
  public final String formName;
  public final String hash;

  /**
   * Create new constructor that needs 3 properties.
   */
  public Key(String mdrName, String recordName, String formName) {
    this.mdrName = KeyFormat.Mdr.replaceVersionAsDefault(mdrName);
    this.recordName = KeyFormat.Record.replaceVersionAsDefault(recordName);
    this.formName = KeyFormat.Form.replaceVersionAsDefault(formName);
    this.hash = this.formName + this.recordName + this.mdrName;

  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    return this.hashCode() == o.hashCode();
  }

  @Override
  public int hashCode() {
    return hash.hashCode();
  }

  /**
   * get the number of this key.
   * @return number
   */
  public long getNumber() {
    Integer mdrNumber = KeyFormat.Mdr.getNumber(mdrName);
    Integer recordNumber = KeyFormat.Record.getNumber(recordName);
    Integer formNumber = KeyFormat.Form.getNumber(formName);
    long hash = 0L;
    if (mdrNumber != null) {
      hash = (long) mdrNumber;
    }
    if (recordNumber != null) {
      hash += (recordNumber * 100000L);
    }
    if (formNumber != null) {
      hash += (formNumber * 100000000L);
    }
    return hash;
  }

}