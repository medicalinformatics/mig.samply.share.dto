package de.samply.share.utils.xmltocsv;

import de.samply.share.common.model.uiquerybuilder.MenuItem;

import java.util.HashMap;

public class Identifier implements Comparable<Identifier> {

  public final Key key;
  public final String definition;
  public final String designation;
  public final Long sortableNumber;

  public boolean isRepeatable = false;


  /**
   * create new constructor Identifier that consists of key and definition and designation.
   */
  public Identifier(Key key, HashMap<String, MenuItem> menuItems) {
    this.key = key;
    MenuItem item = menuItems.get(key.mdrName);
    if (item != null) {
      this.definition = item.getDefinition();
      this.designation = item.getDesignation();
    } else {
      this.definition = "";
      this.designation = "";
    }
    this.sortableNumber = key.getNumber();
  }

  @Override
  public boolean equals(Object o) {
    return this.hashCode() == o.hashCode();
  }

  @Override
  public int hashCode() {
    return sortableNumber.hashCode();
  }

  @Override
  public int compareTo(Identifier name) {
    return this.sortableNumber.compareTo(name.sortableNumber);
  }

}
