package de.samply.share.utils.xmltocsv;

import java.util.HashMap;

public class Episode {

  public final String epiDate;
  public final HashMap<Identifier, Element> epiElements = new HashMap<>();
  public int maxIndex = 0;

  public Episode(String epiDate) {
    this.epiDate = epiDate;
  }

  /**
   * This episode adds one or more mdrElements.
   * Check the FormID whether repeatable mdrElement, duplicate or conflict is detected.
   */
  public void addElement(Identifier elementID, String value, String formID) {
    Element element = epiElements.get(elementID);
    if (element == null) {
      element = new Element(elementID);
      epiElements.put(elementID, element);
    }
    element.addValue(value, formID);
    if (maxIndex < element.values.size()) {
      maxIndex = element.values.size();
    }
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    return this.hashCode() == o.hashCode();
  }

  @Override
  public int hashCode() {
    return epiDate.hashCode();
  }

}
