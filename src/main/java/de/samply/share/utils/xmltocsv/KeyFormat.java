package de.samply.share.utils.xmltocsv;

public enum KeyFormat {
  Form, Record, Mdr;
  public static final boolean ignoreVersion = true;

  /**
   * Each different version number are converted to the same version.
   */
  public String replaceVersionAsDefault(String id) {
    if (!ignoreVersion) {
      return id;
    }

    int endIndex = -1;
    if (Mdr.equals(this) || Record.equals(this)) {
      for (int i = id.length() - 1; i > 0; i--) {
        if (id.charAt(i) == ':') {
          endIndex = i + 1;
          break;
        }
      }

    } else if (Form.equals(this)) {
      for (int i = id.length() - 1; i > 0; i--) {
        if (id.charAt(i) == '-') {
          endIndex = i + 1;
          break;
        }
      }
    }

    if (endIndex > 0) {
      return id.substring(0, endIndex) + "*";
    } else {
      return id;
    }
  }

  /**
   * Each element, record and form are converted into a unique ID.
   */
  public Integer getNumber(String id) {
    if (id == null || id.isEmpty()) {
      return null;
    }
    try {
      if (Mdr.equals(this) || Record.equals(this)) {
        String[] words = id.split(":");
        if ("urn".contains(words[0])) {
          if ("dataelement".contains(words[2]) || "record".contains(words[2])) {
            return Integer.valueOf(words[3]); // is ID of element
          }
        }
      } else if (Form.equals(this)) {
        String[] words = id.split("_");
        if ("form_".contains(words[0])) {
          return Integer.valueOf(words[1]);
        }
      }
    } catch (NullPointerException | NumberFormatException e) {
      //nothing
    }
    return null;
  }

}
