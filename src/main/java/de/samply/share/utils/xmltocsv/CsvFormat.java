package de.samply.share.utils.xmltocsv;

public class CsvFormat {

  private static final String SEPARATOR = ";";
  private static final String CHAR_AT_FIRST = "\"";
  private static final String CHAR_AT_END = "\"";
  public static final String ENTER = "\n";
  private static final String BREAK_LINE = "\r\n";

  public static String getWord(String word) {
    return CHAR_AT_FIRST + word + CHAR_AT_END + SEPARATOR;
  }

  public static StringBuilder getLine(StringBuilder line) {
    return line.append(BREAK_LINE);
  }

}
