package de.samply.share.utils.xmltocsv;

import java.io.IOException;
import java.io.Writer;
import java.util.TreeSet;

public class Headers {

  public static final String PID = "pid";
  public static final String LOCATION = "location";
  public final TreeSet<Identifier> caseElementIDs = new TreeSet<>();
  public static final String EPISODE = "epi-date";
  public final TreeSet<Identifier> epiElementIDs = new TreeSet<>();


  /**
   * These headers are written in Csv.
   */
  public void write(Writer csvWriter) throws IOException {

    final StringBuilder headers0 = new StringBuilder(CsvFormat.getWord(PID));
    final StringBuilder headers1 = new StringBuilder(CsvFormat.getWord(PID));
    final StringBuilder headers2 = new StringBuilder(CsvFormat.getWord(PID));
    final StringBuilder headers3 = new StringBuilder(CsvFormat.getWord(PID));
    final StringBuilder headers4 = new StringBuilder(CsvFormat.getWord(PID));

    headers0.append(CsvFormat.getWord(LOCATION));
    headers1.append(CsvFormat.getWord(LOCATION));
    headers2.append(CsvFormat.getWord(LOCATION));
    headers3.append(CsvFormat.getWord(LOCATION));
    headers4.append(CsvFormat.getWord(LOCATION));

    headers0.append(CsvFormat.getWord(EPISODE));
    headers1.append(CsvFormat.getWord(EPISODE));
    headers2.append(CsvFormat.getWord(EPISODE));
    headers3.append(CsvFormat.getWord(EPISODE));
    headers4.append(CsvFormat.getWord(EPISODE));

    for (Identifier id : caseElementIDs) {
      headers0.append(CsvFormat.getWord("Case-" + id.key.formName));
      headers1.append(CsvFormat.getWord(id.key.recordName));
      if (id.isRepeatable) {
        headers2.append(CsvFormat.getWord(id.key.mdrName + " [repeatable]"));
      } else {
        headers2.append(CsvFormat.getWord(id.key.mdrName));
      }
      headers3.append(CsvFormat.getWord(id.definition));
      headers4.append(CsvFormat.getWord(id.designation));
    }

    for (Identifier id : epiElementIDs) {
      headers4.append(CsvFormat.getWord(id.designation));
      headers3.append(CsvFormat.getWord(id.definition));
      if (id.isRepeatable) {
        headers2.append(CsvFormat.getWord(id.key.mdrName + " [repeatable]"));
      } else {
        headers2.append(CsvFormat.getWord(id.key.mdrName));
      }
      headers1.append(CsvFormat.getWord(id.key.recordName));
      headers0.append(CsvFormat.getWord("Episode-" + id.key.formName));
    }

    csvWriter.append(CsvFormat.getLine(headers0));
    csvWriter.append(CsvFormat.getLine(headers1));
    csvWriter.append(CsvFormat.getLine(headers2));
    csvWriter.append(CsvFormat.getLine(headers3));
    csvWriter.append(CsvFormat.getLine(headers4));

  }

  public int size() {
    return caseElementIDs.size() + epiElementIDs.size() + 3;
  }

  public int casePosition() {
    return 3;
  }

  public int episodePosition() {
    return caseElementIDs.size() + 3;
  }
}