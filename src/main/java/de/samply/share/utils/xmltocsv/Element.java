package de.samply.share.utils.xmltocsv;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Element {

  public final Identifier name;
  public final List<Value> values = new ArrayList<>(); // one value or repeatable values
  public final HashSet<String> formIds = new HashSet<>();

  public Element(Identifier id) {
    this.name = id;
  }


  /**
   * This mdrElement add one or more values.
   * Check the FormID whether repeatable mdrElement, duplicate or conflict is detected.
   */
  public void addValue(String value, String formID) {

    if (values.size() == 0) {
      formIds.add(formID);
      values.add(new Value(value, Value.Is.NORMAL));

    } else if (formIds.contains(formID)) {
      name.isRepeatable = true;
      values.add(new Value(value, Value.Is.REPEATABLE));

    } else if (name.isRepeatable) {
      values.add(new Value(value, Value.Is.CONFLICT)); //conflict, because other ID of form

    } else {
      boolean isDuplicate = false;
      for (Value v : values) { //check each value
        if (v.value.equals(value)) {
          isDuplicate = true;
        } else {
          isDuplicate = false;
          break;
        }
      }
      if (isDuplicate) {
        if (Configuration.FILL_ALL) {
          values.add(new Value(value, Value.Is.NORMAL));
        } else {
          values.add(new Value(value, Value.Is.DUPLICATE));
        }
      } else {
        values.get(0).is = Value.Is.CONFLICT;
        values.add(new Value(value, Value.Is.CONFLICT));
      }
    }
  }

  @Override
  public boolean equals(Object o) {
    if (o == null) {
      return false;
    }
    return this.hashCode() == o.hashCode();
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

}
