/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.share.utils;

import de.samply.share.common.model.uiquerybuilder.MenuItem;
import de.samply.share.model.osse.Attribute;
import de.samply.share.model.osse.Case;
import de.samply.share.model.osse.Container;
import de.samply.share.model.osse.Entity;
import de.samply.share.model.osse.Patient;
import de.samply.share.model.osse.QueryResult;
import de.samply.share.utils.xmltocsv.Identifier;
import de.samply.share.utils.xmltocsv.Key;
import de.samply.share.utils.xmltocsv.KeyFormat;
import de.samply.share.utils.xmltocsv.PatientTable;
import de.samply.share.utils.xmltocsv.Table;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;


public class XmlToCsv {

  private final HashMap<String, MenuItem> menuItems = new HashMap<>();
  private final QueryResult result;
  private final Table table = new Table();
  private final HashMap<Key, Identifier> ids = new HashMap<>();

  /**
   * Create new constructor.
   */
  public XmlToCsv(List<MenuItem> menuItemList, QueryResult result) {
    for (MenuItem item : menuItemList) {
      String mdrId = KeyFormat.Mdr.replaceVersionAsDefault(item.getMdrId());
      this.menuItems.put(mdrId, item);
    }
    this.result = result;

  }

  /**
   * csv file is written by this function.
   *
   * @param csvWriter as file
   */
  public void convert(Writer csvWriter) throws IOException {

    for (Entity entPatient : result.getEntity()) {
      Patient patient = (Patient) entPatient;
      String patientId = patient.getId();
      String location = "";
      for (Attribute attribute : patient.getAttribute()) {
        if ("location".equals(attribute.getMdrKey())) {
          location = attribute.getValue().getValue();
        }
      }
      PatientTable patientTable = new PatientTable(patientId, location);
      table.patients.add(patientTable);

      for (Case cs : patient.getCase()) {

        for (Container container : cs.getContainer()) {

          String caseOrEpisode = container.getDesignation();

          // Case
          if (caseOrEpisode.startsWith("CaseForm:")) {

            String formName = caseOrEpisode.substring("CaseForm:".length());
            String formID = container.getId();

            for (Attribute attr : container.getAttribute()) {
              Key key = new Key(attr.getMdrKey(), "", formName);
              Identifier id = getID(key);
              table.headers.caseElementIDs.add(id);
              patientTable.addCaseElement(id, attr.getValue().getValue(), formID);

            }

            for (Container record : container.getContainer()) {
              if ("Record".equals(record.getDesignation())) {
                String recordName = record.getId();
                for (Attribute attr : record.getAttribute()) {
                  Key key = new Key(attr.getMdrKey(), recordName, formName);
                  Identifier id = getID(key);
                  table.headers.caseElementIDs.add(id);
                  patientTable.addCaseElement(id, attr.getValue().getValue(), formID);
                }
              }
            }

            //Episode
          } else if (caseOrEpisode.startsWith("Episode")) {
            String episodeDate = null;

            for (Attribute attr : container.getAttribute()) {
              if ("episode-date".equals(attr.getMdrKey())) {
                episodeDate = attr.getValue().getValue();
              }
            }

            for (Container episodeForm : container.getContainer()) {

              String formName = episodeForm.getDesignation().substring("EpisodeForm:".length());
              String formID = container.getId();

              for (Attribute attr : episodeForm.getAttribute()) {
                Key key = new Key(attr.getMdrKey(), "", formName);
                Identifier id = getID(key);
                table.headers.epiElementIDs.add(id);
                patientTable.addEpisodeElement(episodeDate, id, attr.getValue().getValue(), formID);
              }

              for (Container record : episodeForm.getContainer()) {
                String recordName = record.getId();
                for (Attribute attr : record.getAttribute()) {
                  Key key = new Key(attr.getMdrKey(), recordName, formName);
                  Identifier id = getID(key);
                  table.headers.epiElementIDs.add(id);
                  patientTable
                      .addEpisodeElement(episodeDate, id, attr.getValue().getValue(), formID);

                }

                for (Container subRecord : record.getContainer()) {
                  throw new IllegalArgumentException("It doesn not support sub-record");
                }
              }
            }
          }
        }
      }
    }

    table.create(csvWriter);
  }

  private Identifier getID(Key key) {
    Identifier id = ids.get(key);
    if (id == null) {
      id = new Identifier(key, menuItems);
      ids.put(key, id);
    }
    return id;
  }

}
