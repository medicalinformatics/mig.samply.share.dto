# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.1.0] - 2020-07-28
### Fixed
- CSV export

## [4.0.1] - 2020-05-26
### Fixed
- checkstyle violations (does not apply to this release though since the checkstyle violations were
 removed beforehand. For the sake of completeness, it is still released)

## [4.0.0] - 2020-05-26
### Added
- proskive view url added to inquiry.xsd
### Changed
- apply google java code style

## [3.0.1] - 2019-01-11
### Added
- Initial release

